$(function () {
  // $("[data-toggle='tooltip']").tooltip();
  $("[data-bs-toggle='popover']").popover();
  $(".carousel").carousel({
    interval: 3000,
  });
});

// Beach
$(function () {
  $("#reserve").on("show.bs.modal", function (e) {
    console.log("opening the modal window");
    $("#reserveBtn").prop("disabled", true);
    $("#reserveBtn").removeClass("btn-success");
    $("#reserveBtn").addClass("btn-secondary");
  });
  $("#reserve").on("shown.bs.modal", function (e) {
    console.log("opened modal window");
  });
  $("#reserve").on("hide.bs.modal", function (e) {
    console.log("closing the modal window");
  });
  $("#reserve").on("hidden.bs.modal", function (e) {
    console.log("closed modal window");
    $("#reserveBtn").prop("disabled", false);
    $("#reserveBtn").toggleClass("btn-success");
  });
});
// Details
$(function () {
  // $("[data-toggle='tooltip']").tooltip();
  $("[data-bs-toggle='popover']").popover();
  $(".carousel").carousel({
    interval: 3000,
  });
});
